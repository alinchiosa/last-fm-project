import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../homepage/homepage.service';
import { ActivatedRoute, ParamMap} from '@angular/router';




@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css'],
  providers: [HomepageService]
})
export class ArtistComponent implements OnInit {

  public tracks = [];
  public trackId: String; 
  selectedArtistId: String;
  public artist;
  

  constructor(
    private homepageService : HomepageService,
    private route : ActivatedRoute,
    
    ) {     }


  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        this.selectedArtistId = params.get('id');
        this.getArtistiInfo();
        this.getTopTracks();
      }
    )
  }

  public getArtistiInfo(){
    this.homepageService.getInfo(this.selectedArtistId).subscribe(
      (data) => {
        this.artist = data['artist'];
        console.log(this.artist);
      },
      (err) => console.log(err)
    );
  }

  public getTopTracks(){
    this.homepageService.getBestTracks(this.selectedArtistId).subscribe(
      (data) => {
        for(let i = 0; this.tracks.length < 5; i++){
          if(data['toptracks']['track'][i].mbid){
            this.tracks.push(data['toptracks']['track'][i]);
          }
        }
        console.log(this.tracks)
      },
      (err) => console.log(err)
    );
  }
}
