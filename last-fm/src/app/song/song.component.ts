import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap} from '@angular/router';
import { HomepageService } from '../homepage/homepage.service';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css'],
  providers: [HomepageService]
})
export class SongComponent implements OnInit {

  @Input() trackId: String;
  selectedId: String;
  public trackInfo: Object;

  constructor(
    private homepageService : HomepageService,
    private route : ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      params => {
        this.selectedId = params.get('id');
      }
    )
    if(this.trackId){
      this.getSongInfo(this.trackId)
    }else{
      this.getSongInfo(this.selectedId);
      this.selectedId=""
    }
  }

  public getSongInfo(id: String){
    this.homepageService.getTrackInfo(id).subscribe(
      (data) => {
        this.trackInfo = data;
        console.log(this.trackInfo)
      }
    )
  }


}
