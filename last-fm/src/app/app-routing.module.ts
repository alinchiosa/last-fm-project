import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { ArtistComponent } from './artist/artist.component';
import { SongComponent } from './song/song.component';

const routes: Routes = [
    {path: '', component: HomepageComponent},
    {path: 'artist/:id', component: ArtistComponent},
    {path: 'topSong/artist/:id', component: SongComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {}