import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  private key = '60f2df3cf840cf2c00f0d10b00cdd957';
  private link = 'http://ws.audioscrobbler.com/2.0/?method='

  constructor(private Http : HttpClient) { }

  public search (singer: String){
    return this.Http.get(this.link + 'artist.search&artist=' + singer + '&api_key=' + this.key + '&format=json')
  }

  public getTopArtists(){
    return this.Http.get(this.link + 'chart.gettopartists&api_key=' + this.key +  '&format=json')
  }

  public getInfo(id: String){
    return this.Http.get(this.link + 'artist.getinfo&mbid=' + id + '&api_key=' + this.key + '&format=json')
  }

  public getBestTracks(id: String){
    return this.Http.get(this.link + "artist.gettoptracks&mbid=" + id + "&api_key=" + this.key +"&format=json")
  }

  public getTrackInfo(id: String){
    return this.Http.get(this.link + "track.getInfo&api_key=" + this.key + "&mbid=" + id + "&format=json")
  }
}
