import { Component, OnInit } from '@angular/core';
import { HomepageService } from './homepage.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css'],
  providers: [HomepageService]
})
export class HomepageComponent implements OnInit {

  public searchSinger = '';
  public results = [];
  public topTrack: String;
  displayedColumns: string[] = ['name', 'listeners', 'mbid', 'bestTrack'];

  constructor(
    private router: Router,
    private homepageService : HomepageService
    ) { }

  ngOnInit() {
    this.homepageService.getTopArtists().subscribe(
      (data) => {
        this.results = data['artists']['artist'];
        console.log(this.results);
      },
      (err) => console.log(err)
    );
  }

  public search(){
    this.homepageService.search(this.searchSinger).subscribe(
      (data) => {
        this.results = data['results']['artistmatches']['artist'];
        console.log(this.results);
      },
      (err) => console.log(err)
    )
  }

  public getTopTrackId(id: String){
    this.homepageService.getBestTracks(id).subscribe(
     (data) => {
       for(let i = 0; i <= data['toptracks']['track'].length; i++){
         if(data['toptracks']['track'][i].mbid){
          this.topTrack = data['toptracks']['track'][i].mbid;
          break;
         }
       }
       this.router.navigateByUrl('topSong/artist/' + this.topTrack);
       console.log(this.topTrack);
     },
     (err) => console.error(err) 
    )
  }

}
